//
//  TextInputTableViewCell.h
//  iKea Shopping List
//
//  Created by Wael Showair on 2015-11-08.
//  Copyright © 2015 showair.wael@gmail.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextInputTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *inputText;
@end
